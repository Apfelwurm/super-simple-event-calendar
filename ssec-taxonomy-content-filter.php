<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Replace the content of our taxonomy and single post.
 *
 * @since 1.0.0
 */
function ssec_content_filter( $content ) {

	if ( is_admin() ) {
		return $content;
	}

	if ( is_tax('ssec_season') && is_main_query() ) {
		$content .= '<table class="ssec-contentfilter-calendar"><tbody>';
		$postlink = '';
		if ( is_user_logged_in() ) {
			$postlink = get_edit_post_link( get_the_ID() );
			if ( strlen( $postlink ) > 0 ) {
				$postlink = ' <a class="post-edit-link" href="' . esc_attr( $postlink ) . '">' . esc_html__('(edit)', 'super-simple-event-calendar') . '</a>';
			}
		}
		$classes = ssec_get_term_classes( get_the_ID() );
		$content .= '
			<tr class="' . esc_attr( $classes ) . '">
				<td class="ssec-title">' . get_the_title() . '</td>
				<td class="ssec-content">' . nl2br(get_the_content()) . $postlink . '</td>
			</tr>
			';
		$content .= '</tbody></table>';
		return $content;
	}

	$post_type = get_post_type();
	if ( $post_type === 'ssec_event' && is_singular() ) {
		$content .= '<table class="ssec-contentfilter-calendar"><tbody>';
		$postlink = '';
		if ( is_user_logged_in() ) {
			$postlink = get_edit_post_link( get_the_ID() );
			if ( strlen( $postlink ) > 0 ) {
				$postlink = ' <a class="post-edit-link" href="' . esc_attr( $postlink ) . '">' . esc_html__('(edit)', 'super-simple-event-calendar') . '</a>';
			}
		}
		$classes = ssec_get_term_classes( get_the_ID() );
		$content .= '
			<tr class="' . esc_attr( $classes ) . '">
				<td class="ssec-title">' . get_the_title() . '</td>
				<td class="ssec-content">' . nl2br(get_the_content()) . $postlink . '</td>
			</tr>
			';
		$content .= '</tbody></table>';
		return $content;
	}

	return $content;

}
add_filter( 'the_content', 'ssec_content_filter', 12 );
