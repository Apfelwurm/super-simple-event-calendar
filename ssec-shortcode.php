<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Shortcode to be used in the content. Displays a simple calendar in a table.
 *
 * Parameters:
 *   - tax_query season parameters for term_ids (since 1.4.0).
 *   - posts_per_page integer, default -1 (all posts) (since 1.4.1).
 *
 * @since 1.0.0
 */
function ssec_shortcode( $atts ) {

	$shortcode_atts = shortcode_atts( array( 'posts_per_page' => -1 ), $atts );
	$posts_per_page = (int) $shortcode_atts['posts_per_page'];
	if ( $posts_per_page === -1 ) {
		$nopaging = true;
	} else {
		$nopaging = false;
	}

	$output = '';

	$tax_query = array();
	if ( ! empty( $atts['season'] ) ) {
		$cat_in = explode( ',', $atts['season'] );
		$cat_in = array_map( 'absint', array_unique( (array) $cat_in ) );
		if ( ! empty( $cat_in ) ) {
			$tax_query['relation'] = 'OR';
			$tax_query[] = array(
				'taxonomy'         => 'ssec_season',
				'terms'            => $cat_in,
				'field'            => 'term_id',
				'include_children' => true,
			);
		}
	}

	$args = array(
		'post_type'      => 'ssec_event',
		'post_status'    => 'future',
		'posts_per_page' => (int) $posts_per_page,
		'nopaging'       => (bool) $nopaging,
		'orderby'        => 'date',
		'order'          => 'ASC',
		'tax_query'      => $tax_query,
	);

	// The Query
	$the_query = new WP_Query( $args );

	// The Loop
	if ( $the_query->have_posts() ) {
		$output .= '
		<table class="ssec-shortcode-calendar">
		<tbody>';

		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$postlink = '';
			if ( is_user_logged_in() ) {
				$postlink = get_edit_post_link( get_the_ID() );
				if ( strlen( $postlink ) > 0 ) {
					$postlink = ' <a class="post-edit-link" href="' . esc_attr( $postlink ) . '">' . esc_html__('(edit)', 'super-simple-event-calendar') . '</a>';
				}
			}
			$classes = ssec_get_term_classes( get_the_ID() );
			$output .= '
			<tr class="' . esc_attr( $classes ) . '">
				<td class="ssec-title">' . get_the_title() . ' </td>
				<td class="ssec-content">' . nl2br(get_the_content()) . $postlink . '</td>
			</tr>';
		}
		$output .= '
		</tbody>
		</table>';

	} else {
		// no posts found
		$output .= esc_html__( 'No future events found', 'super-simple-event-calendar' );
	}
	/* Restore original Post Data */
	wp_reset_postdata();

	return $output;

}
add_shortcode( 'super_simple_event_calendar', 'ssec_shortcode' );
